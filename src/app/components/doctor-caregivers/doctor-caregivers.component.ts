import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CaregiverControllerService, Patient, PatientControllerService } from 'build/openapi';
import { Caregiver } from 'build/openapi/model/caregiver';


@Component({
  selector: 'app-doctor-caregivers',
  templateUrl: './doctor-caregivers.component.html',
  styleUrls: ['./doctor-caregivers.component.css']
})

/*    id?: number;
    name?: string;
    date?: string;
    gender?: Caregiver.GenderEnum;
    address?: string;
    patients?: Array<Patient>;
    password?: string;
    */
export class DoctorCaregiversComponent implements OnInit {  
    truePatients: Patient[] =[];
    falsePatients: Patient[] =[];
    newCaregiver:Caregiver ={
      name: '',
      date: '',
      gender: Caregiver.GenderEnum.Male,
      address: '',
      password: '',
      patients: []
    };
    newCaregiver2:Caregiver = {};
    genderList;
    action : boolean = false;
    visible:boolean = false;
    editCreate:boolean = true;
    caregivers:Caregiver[] = [];
    ws:WebSocket
    constructor(private router:Router,private caregiverService:CaregiverControllerService, private patientService: PatientControllerService) { }
    ngOnInit(): void {
      this.ws = new WebSocket("ws://localhost:8080/endpoint");
      var self = this
      this.ws.onmessage = function (event) {
        console.log('ajunge')
        var str1 = event.data.substr(0,event.data.indexOf(' '))
        var str2 = event.data.substr(event.data.indexOf(' ')+1)
        console.log(str1)
        console.log(str2)
        document.getElementById('p1').innerHTML = ''
        if(str1.substr(0,1) === 'm')
        document.getElementById('p1').innerHTML = str2
      }

      if(sessionStorage.getItem('authenticatedUserRole')!= "D")this. navigateLogin()
      this.caregiverService.getAllCaregivers().subscribe(x=>this.caregivers = x);
      this.genderList = Object.keys(Caregiver.GenderEnum);
    }
    reloadTable(){
      this.caregiverService.getAllCaregivers().subscribe(x=>this.caregivers = x);
    }
  
    navigateLogin(){
      this.router.navigate(['/login']);
    }
    createCaregiver(){
      this.truePatients = [];
      this.patientService.getAllPatients().subscribe(x=>this.falsePatients = x);
      this.visible=true;
      this.newCaregiver.idCaregiver = null;
      this.action= true;
    }
  
    editCaregiver(selectedCaregiver: Caregiver){
      this.patientService.getPatientsByCaregiver(selectedCaregiver.idCaregiver).subscribe( x=> this.truePatients = x);
      this.patientService.getPatientsNotCaregiver(selectedCaregiver.idCaregiver).subscribe(x=>this.falsePatients = x);
      this.visible = true;
      this.newCaregiver = selectedCaregiver;
      this.action = false;
    }
    
    modifyCurrentPatients(patient: Patient){
      if(this.truePatients.some(x=>x.idPatient ===patient.idPatient))
      {
        this.truePatients.splice(this.truePatients.indexOf(patient),1);
        this.falsePatients.push(patient);
      
      }
      else
      {
        this.falsePatients.splice(this.falsePatients.indexOf(patient),1);
        this.truePatients.push(patient);
      }
    }

    deleteCaregiver(id: number){
      this.caregiverService.deleteCaregiver(id).subscribe(
        x=>this.caregiverService.getAllCaregivers().subscribe(x=>this.caregivers = x)
      )
    }
   
    navigateDoctorMain(){
      this.router.navigate(['doctor/main']);
    }
  
    onSubmit(){
      
     // console.log(this.newCaregiver)
      if(this.newCaregiver.idCaregiver == null){
        this.newCaregiver.patients = []
        this.caregiverService.createCaregiver(this.newCaregiver).subscribe(givenCaregiver=>{
          this.newCaregiver = givenCaregiver;
          this.newCaregiver.patients = this.truePatients;
          this.caregiverService.editCaregiver(this.newCaregiver).subscribe(x=>{
            this.reloadTable();this.visible= false;
            this.visible = false;
            })
        })
      }
      else{
      this.newCaregiver.patients = this.truePatients;
      this.caregiverService.editCaregiver(this.newCaregiver).subscribe(x=>{
      this.reloadTable();this.visible= false;
      this.visible = false;
      })}
    }
  
    cancel(){
      this.visible = false;
      this.reloadTable();
    }
  
  }
  
