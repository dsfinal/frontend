import { DepFlags } from '@angular/compiler/src/core';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MedInfoControllerService, PatientControllerService } from 'build/openapi';
import { MedicationControllerService } from 'build/openapi/api/medicationController.service';
import { SideEffectControllerService } from 'build/openapi/api/sideEffectController.service';
import { MedInfo } from 'build/openapi/model/medInfo';
import { Patient } from 'build/openapi/model/patient';
import { pathToFileURL } from 'url';

@Component({
  selector: 'app-caregiver-main',
  templateUrl: './caregiver-main.component.html',
  styleUrls: ['./caregiver-main.component.css']
})
export class CaregiverMainComponent implements OnInit {
  
    currentMedInfos: MedInfo[];
    patients: Patient[] = [];
    visible: boolean = false;
    ws : WebSocket;
    constructor(private router:Router, private medInfoService:MedInfoControllerService, private patientService: PatientControllerService, private route: ActivatedRoute) {

   
     }
  
    ngOnInit(): void {
      this.ws = new WebSocket("ws://localhost:8080/endpoint");
      var self = this
      this.ws.onmessage = function (event) {
        var str1 = event.data.substr(0,event.data.indexOf(' '))
        var str2 = event.data.substr(event.data.indexOf(' ')+1)
        console.log(event.data)
        console.log(str1)
        console.log(str2)
        document.getElementById('p1').innerHTML = ''
        self.patients.forEach(x=>{
          if(str1.substr(1) === x.idPatient.toString()){
          document.getElementById('p1').innerHTML = str2
          console.log('ajunge2')
        }
        })
      }
      document.getElementById("p1").innerHTML = "";
      if(sessionStorage.getItem('authenticatedUserRole') != "C"/*|| (this.route.params['id']!= sessionStorage.getItem('authenticatedUserID'))*/) this.navigateLogin();
      this.patientService.getPatientsByCaregiver(this.route.snapshot.params['id']).subscribe(x=>{this.patients = x})
    }
    navigateLogin(){
      this.router.navigate(['/login']);
    }
    viewPrescription(id:number){
      this.medInfoService.getMedInfosByPatient(id).subscribe(x=>this.currentMedInfos =x);
      this.visible = true;
    }
    cancel(){
      this.visible = false;
    }
  }  
