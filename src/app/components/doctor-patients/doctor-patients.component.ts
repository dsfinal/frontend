import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Patient, PatientControllerService } from 'build/openapi';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-doctor-patients',
  templateUrl: './doctor-patients.component.html',
  styleUrls: ['./doctor-patients.component.css']
})
export class DoctorPatientsComponent implements OnInit {

  ws:WebSocket
  newPatient:Patient ={
    name: '',
    date: '',
    gender: Patient.GenderEnum.Male,
    address: '',
    password: '',
    record: ''
  };
  genderList;
  action : boolean = false;
  visible:boolean = false;
  editCreate:boolean = true;
  patients:Patient[] = [];
  constructor(private router:Router,private patientService:PatientControllerService) { }
  ngOnInit(): void {
    this.ws = new WebSocket("ws://localhost:8080/endpoint");
    var self = this
    this.ws.onmessage = function (event) {
      console.log('ajunge')
      var str1 = event.data.substr(0,event.data.indexOf(' '))
      var str2 = event.data.substr(event.data.indexOf(' ')+1)
      console.log(str1)
      console.log(str2)
      document.getElementById('p1').innerHTML = ''
      if(str1.substr(0,1) === 'm')
      document.getElementById('p1').innerHTML = str2
    }

    if(sessionStorage.getItem('authenticatedUserRole')!= "D")this. navigateLogin()
    this.patientService.getAllPatients().subscribe(x=>this.patients = x);
    this.genderList = Object.keys(Patient.GenderEnum);
  }
  reloadTable(){
    this.patientService.getAllPatients().subscribe(x=>this.patients = x);
  }
  navigateLogin(){
    this.router.navigate(['/login']);
  }
  createPatient(){
    this.visible=true;
    this.newPatient.idPatient = null;
    this.action= true;
  }

  editPatient(selectedPatient: Patient){
    this.visible = true;
    this.newPatient = selectedPatient;
    this.action = false;
  }

  deletePatient(id: number){
    this.patientService.deletePatient(id).subscribe(
      x=>this.patientService.getAllPatients().subscribe(x=>this.patients = x)
    )
  }

  prescribePatient(id:number){
    this.router.navigate(['doctor/patient/prescription',id]);
  }
 
  navigateDoctorMain(){
    this.router.navigate(['doctor/main']);
  }
  onSubmit(){
    this.patientService.createPatient(this.newPatient).subscribe(x=>this.reloadTable())
    this.visible= false;
  }

  cancel(){
    this.visible = false;
    this.reloadTable();
  }

}
