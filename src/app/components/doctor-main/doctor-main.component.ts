import { Component, OnInit } from '@angular/core';
import { fromEventPattern } from 'rxjs';
import { Router} from '@angular/router';

@Component({
  selector: 'app-doctor-main',
  templateUrl: './doctor-main.component.html',
  styleUrls: ['./doctor-main.component.css']
})
export class DoctorMainComponent implements OnInit{
  ws:WebSocket

  constructor(private router:Router) {}
  ngOnInit(): void {
    this.ws = new WebSocket("ws://localhost:8080/endpoint");
      var self = this
      this.ws.onmessage = function (event) {
        console.log('ajunge')
        var str1 = event.data.substr(0,event.data.indexOf(' '))
        var str2 = event.data.substr(event.data.indexOf(' ')+1)
        console.log(str1)
        console.log(str2)
        document.getElementById('p1').innerHTML = ''
        if(str1.substr(0,1) === 'm')
        document.getElementById('p1').innerHTML = str2
      }

    if(sessionStorage.getItem('authenticatedUserRole')!= "D")this. navigateLogin()
  }
  navigateLogin(){
    this.router.navigate(['/login']);
  }
  navigatePatients(){
    this.router.navigate(['doctor/patients']);
  }
  navigateCaregivers(){
    this.router.navigate(['doctor/caregivers']);
  }
  navigateMedication(){
    this.router.navigate(['doctor/medication']);
  }

  navigateAddMedication(){
    this.router.navigate(['doctor/add/medication']);
  }

  navigateAddCaregiver(){
    this.router.navigate(['doctor/add/caregiver']);
  }
}