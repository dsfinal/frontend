import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from './components/login-page/userSarma';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  SESSION_ATTRIBUTE_ID = 'authenticatedUserID'
  SESSION_ATTRIBUTE_ROLE = 'authenticatedUserRole'
  SESSION_ATTRIBUTE_TOKEN = 'authenticatedUserToken'
  id : number; 
  role: String;
  constructor(private http:HttpClient) {}
  user: User ={
    username : '',
    password : ''
  }

  authenticate(username:string,password:string) : Observable<any>{
    this.user.username =username
    this.user.password = password
    return this.http.post<any>('http://localhost:8080/api/authenticate',this.user).pipe(map(
      x=>{
      this.doLoginUser(x.jwt)
      return of(true)
      }))
  }

  doLoginUser(token){
    sessionStorage.setItem(this.SESSION_ATTRIBUTE_TOKEN,token)
    console.log(sessionStorage.getItem(this.SESSION_ATTRIBUTE_TOKEN))
  }

  registerSuccessfulLogin(id : number, role: string){
    sessionStorage.setItem(this.SESSION_ATTRIBUTE_ID,id.toString())
    sessionStorage.setItem(this.SESSION_ATTRIBUTE_ROLE,role);
    this.id = id;
    this.role = role;
  }

  logout() {
    sessionStorage.removeItem(this.SESSION_ATTRIBUTE_ID);
    sessionStorage.removeItem(this.SESSION_ATTRIBUTE_TOKEN);
    sessionStorage.removeItem(this.SESSION_ATTRIBUTE_TOKEN);
    this.id = null;
    this.role = null;
  }

  isLogged():boolean{
    let user = sessionStorage.getItem(this.SESSION_ATTRIBUTE_ID)
    
    if (user === null )return false;
    return true
  }

  getJwtToken(){
    return sessionStorage.getItem(this.SESSION_ATTRIBUTE_TOKEN)
  }
}
