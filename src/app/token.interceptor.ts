import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { LoginServiceService } from "./login-service.service";






@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    constructor(public authService: LoginServiceService){}
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if(this.authService.getJwtToken()){
            req = this.addToken(req,this.authService.getJwtToken());
            console.log(req.headers)
        }
        return next.handle(req).pipe(catchError(error=>{
                    return throwError(error);
        }))
    }

    addToken(request: HttpRequest<any>,token:string){
        return request.clone({
            setHeaders:{
                'Authorization': 'Bearer ' + token
            }
        })
    }
}